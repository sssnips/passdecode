#!/bin/sh

set -e

PASSFILE=${PASSFILE-'passwords.txt'}
GPGFILE=${GPGFILE-"$PASSFILE.gpg"}
GPG=${GPG-'gpg'}
GPG_CMD=${GPG_ARGS-'-d'}
GPG_ARGS=${GPG_ARGS-''}

if [ -e "$PASSFILE" ]; then
	echo "The passwords file $PASSFILE already exists" 1>&2
	exit 1
fi
if [ ! -r "$GPGFILE" ]; then
	echo "The encrypted passwords file $GPGFILE does not exist" 1>&2
	exit 1
fi

$GPG $GPG_CMD $GPG_ARGS "$GPGFILE" > "$PASSFILE"
