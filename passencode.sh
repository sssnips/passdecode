#!/bin/sh

set -e

passfile=${passfile-'passwords.txt'}
gpgfile=${gpgfile-"$passfile.gpg"}
gpgbak=${gpgbak-"$gpgfile.backup"}
gpg=${gpg-'gpg'}
gpg_cmd=${gpg_args-'-e'}
gpg_args=${gpg_args-''}
diff=${diff-'diff'}
diff_args=${diff_args-'-u'}
mv=${mv-'mv'}
rm=${rm-'rm'}

if [ ! -e "$passfile" ]; then
	echo "The passwords file $passfile does not exist" 1>&2
	exit 1
fi
if [ ! -r "$gpgfile" ]; then
	echo "The encrypted passwords file $gpgfile does not exist" 1>&2
	exit 1
fi

$mv "$gpgfile" "$gpgbak"
$gpg $gpg_cmd $gpg_args "$passfile"

if [ ! -e "$gpgfile" ]; then
	echo "The encryption seems to have failed" 1>&2
	$mv "$gpgbak" "$gpgfile"
else
	$rm "$passfile" "$gpgbak"
fi
