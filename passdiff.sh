#!/bin/sh

set -e

PASSFILE=${PASSFILE-'passwords.txt'}
GPGFILE=${GPGFILE-"$PASSFILE.gpg"}
GPG=${GPG-'gpg'}
GPG_CMD=${GPG_ARGS-'-d'}
GPG_ARGS=${GPG_ARGS-''}
DIFF=${DIFF-'diff'}
DIFF_ARGS=${DIFF_ARGS-'-u'}

if [ ! -e "$PASSFILE" ]; then
	echo "The passwords file $PASSFILE does not exist" 1>&2
	exit 1
fi
if [ ! -r "$GPGFILE" ]; then
	echo "The encrypted passwords file $GPGFILE does not exist" 1>&2
	exit 1
fi

$GPG $GPG_CMD $GPG_ARGS "$GPGFILE" | $DIFF $DIFF_ARGS - "$PASSFILE"
